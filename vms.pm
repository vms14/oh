### Public domain.

use strict;

use warnings;

package vms;

use Term::ANSIColor;

use Exporter qw/import/;

use List::Util qw/sum0 max maxstr min minstr/;

use IO::Socket;

use DB_File;

use Scalar::Util qw/reftype/;

use threads;
use Thread::Queue;

my $queue = Thread::Queue->new;

our @EXPORT_OK = qw/word put get getn get2 get3 get4 interpret_element interpret_atom interpret_list value pkg find pr interpolate_string interpret_string interpret_file parent execute_word colon read_word read_source read_error hash_or_array repl attach detach/;

my @stack;

my %packages = ( main => {} );

my %searching;

my $current_package = 'main';

my %parents;

my $env = {};

my $source;

my $out;

my $stdin = *STDIN;

my $stdout = *STDOUT;

my $stderr = *STDERR;

my $last_atom;

my $last_word;

my $recurse;

sub error
{
    print "@_\n";
    print "last atom: $last_atom\nlast word: $last_word\n";
}
sub listp
{
    ref (shift()) eq 'ARRAY';
}

sub word (&$)
{
    my ($code, $name) = @_;
    $packages{$current_package}->{$name} = $code;
}

sub search
{
    my $word = shift;
    my $package = shift || $current_package;
    if (exists $packages{$package})
    {
        if (exists $packages{$package}->{$word})
        {
            $packages{$package}->{$word};
        }
        elsif (exists $parents{$package})
        {
            my $found;
            for my $parent (@{$parents{$package}})
            {
                next if $searching{$parent};
                $searching{$parent} = 1;
                $found = search($word, $parent);
                last if $found;
            }
            $found;
        }
        else
        {
            undef;
        }
    }
    else
    {
        error "searching word: $word in package $package, the package does not exist"
    }
}

sub find
{
    %searching = ();
    my $word = shift;
    $env->{$word} // search($word) // $packages{main}->{$word};
}

sub pkg
{
    my $pkg = shift;
    if (not exists $packages{$pkg})
    {
        $packages{$pkg} = {};
    }
    $current_package = $pkg;
}

sub parent
{
    my $parent = shift;
    my $package = shift || $current_package;
    if (exists $parents{$package})
    {
        push @{$parents{$package}}, $parent;
    }
    else
    {
        $parents{$package} = [$parent];
    }
}

sub put
{
    push @stack, @_;
}

sub get
{
    if (@stack)
    {
        pop @stack;
    }
    else
    {
        error 'stack underflow';
    }
}

sub getn
{
    my $num = shift;
    if (@stack >= $num)
    {
        splice @stack, $num * -1;
    }
    else
    {
        error 'stack underflow';
    }
}

sub get2
{
    if (@stack > 1)
    {
        splice @stack, -2;
    }
    else
    {
        error 'stack underflow';
    }
}

sub get3
{
    if (@stack > 2)
    {
        splice @stack, -3;
    }
    else
    {
        error 'stack underflow';
    }
}

sub get4
{
    if (@stack > 3)
    {
        splice @stack, -4;
    }
    else
    {
        error 'stack underflow';
    }
}

sub execute_word
{
    my $word = shift;
    my $type = ref $word;
    
    if ($type eq 'CODE')
    {
        $word->();
    }
    elsif ($type eq 'colon')
    {
        my $old = $env;
        my %env = ( %$env, %{$word->{env}} );
        $env = \%env;
        interpret_list($word->{code});
        $env = $old;
    }
    else
    {
        put $word;
    }
}

sub interpret_element
{
    my $element = shift;
    if (not defined $element)
    {
        put undef;
        return;
    }
    my $ref = ref $element;

    if ($ref)
    {
        if ($ref eq 'ARRAY')
        {
            interpret_list ($element);
        }
        else
        {
            put $element;
        }
    }
    else
    {
        
        my $found = find $element;

        if (defined $found)
        {
            $last_word = $element;
            execute_word ($found);
        }
        else
        {
            $last_atom = $element;
            interpret_atom ($element);
        }

    }
    
}

sub interpret_list
{
    for my $element (@{shift()})
    {
        if (listp $element)
        {
            put $element;
        }
        else
        {
            interpret_element $element;
        }
    }
}

sub interpret_atom
{
    my $atom = shift;

    if ($atom =~ /^\'/)
    {
        put substr($atom,1);
    }
    elsif ($atom =~ /^:/)
    {
        $env->{substr($atom,1)} = get;
    }
    elsif ($atom =~ /^\".*\"$/s)
    {
        put substr($atom,1,-1);
    }
    elsif ($atom =~ /^\-?\d+\.?\d*$/)
    {
        put $atom;
    }
    elsif ($atom =~ /\/.+\/.*/)
    {
        my $string = get;
        my @values = eval "(\$string =~ $atom)";

        if (@values > 1)
        {
            put \@values;
        }
        else
        {
            put $values[0];
        }
    }
    elsif ($atom =~ /^\-/)
    {
        my $method_name = substr $atom,1;
        my @args;
        if ($atom =~ /\*$/)
        {
            @args = hash_or_array(get());
            $method_name = substr $method_name,0,-1;
        }
        my $obj = get;

        my $method = $obj->can($method_name);
        if ($method)
        {
            my @result = $method->($obj,@args);
            if (@result)
            {
                if (@result == 1)
                {
                    put @result;
                }
                else
                {
                    put \@result;
                }
            }
            else
            {
                put undef;
            }
        }
        else
        {
            error "$method_name method does not seem to exist in " . (ref $obj || $obj);
        }
    }
    elsif ($atom =~ /^\~/)
    {
        my $method_name = substr $atom,1;
        my @args;
        if ($atom =~ /\*$/)
        {
            @args = hash_or_array(get());
            $method_name = substr $method_name,0,-1;
        }
        my $obj = get;

        my $method = $obj->can($method_name);
        if ($method)
        {
            $method->($obj,@args);
        }
        else
        {
            error "$method_name does not seem to exist in object " . ref $obj;
        }
    }
    elsif ($atom =~ /^\..+/)
    {
        if ($atom =~ /\!$/)
        {
            my ($hash_or_list, $value) = get2;
            my $ref = reftype $hash_or_list || '';
            if ($ref eq 'HASH')
            {
                $hash_or_list->{substr($atom,1,-1)} = $value;
            }
            elsif ($ref eq 'ARRAY')
            {
                $hash_or_list->[substr($atom,1,-1)] = $value;
            }
            else
            {
                error '.! assignment in element not a hash or list';
            }
            
        }
        else
        {
            my $hash_or_list = get;
            my $ref = reftype $hash_or_list || '';
            if ($ref eq 'HASH')
            {
                put($hash_or_list->{substr($atom,1)});
            }
            elsif ($ref eq 'ARRAY')
            {
                put($hash_or_list->[substr($atom,1)]);
            }
            else
            {
                error 'trying to access an element not being a hash or list';
            }

        }
    }
    elsif ($atom =~ /.+::.+/)
    {
        my ($package, $word) = split(/::/,$atom);
        if (exists $packages{$package})
        {
            if (exists $packages{$package}->{$word})
            {
                execute_word $packages{$package}->{$word};
            }
            else
            {
                error "$word not found in $package package";
            }
        }
        else
        {
            error "package $package does not exist";
        }
    }
    elsif ($atom =~ /^0x[-f0-9]+/i)
    {
        put hex($atom);
    }
    else
    {
        error "$atom not recognized";
    }
}

sub colon
{
    my ($name, $list, %env) = @_;
    $packages{$current_package}->{$name} = bless { code => $list, env => \%env }, 'colon';
}

sub read_word
{
    my $word = '';
    my $char = getc $source;
    while (defined $char and $char =~ /\s/s)
    {
        $char = getc $source;
    }

    while (defined $char and $char !~ /\s/s)
    {
        $word .= $char;
        $char = getc $source;
    }

    if ($word eq '')
    {
        undef;
    }
    else
    {
        $word;
    }
}

sub interpret_source
{
    my $word = read_word;

    while (defined $word)
    {
        interpret_element $word;
        $word = read_word;
    }
}

sub interpret_string
{
    my $string = shift;
    open my $fh, '<', \$string or error "can't open string $string to read as source code" and return;
    my $old = $source;
    $source = $fh;
    interpret_source;
    $source = $old;
}

sub interpret_file
{
    my $filename = shift;
    open my $fh, '<', $filename or error "can't open file $filename to read as source code" and return;
    my $old = $source;
    $source = $fh;
    interpret_source;
    $source = $old;
}

sub repl
{
    my $line;
    interpret_element 'prompt';
    while (defined ($line = <>) and $line ne "\n")
    {
        chomp $line;
        eval { interpret_string($line) };
        if ($@)
        {
            error $@;
        }
        interpret_element '.s';
        interpret_element 'prompt';
    }
}

sub read_error
{
    my ($element, $end, $start) = @_;
    if (defined $element)
    {
        0;
    }
    else
    {
        $start = $start || $end;
        error "$start word did not find a terminating $end";
        1;
    }

}

sub interpolate_string
{
    my @chars = split //,shift;
    my $string = '';

    while (@chars)
    {
        my $char = shift @chars;
        if ($char eq '~')
        {
            $char = shift @chars;
            if (not defined $char)
            {
                error 'format found a ~ directive at the end of a string'
            }
            if ($char eq 'a')
            {
                my $item = get;
                if (listp $item)
                {
                    my $separator = find('separator');
                    if ($separator)
                    {
                        execute_word $separator;
                        $separator = get;
                    }
                    $string .= join($separator || '', @$item);
                }
                else
                {
                    $string .= $item;
                }
                
            }
            elsif ($char eq 'n')
            {
                $string .= "\n";
            }
            else
            {
                $string .= $char;
            }
        }
        else
        {
            $string .= $char;
        }
    }

    $string;
    
}

sub hash_or_array
{
    my $item = shift;
    my $ref = reftype $item || '';
    
    if ($ref eq 'HASH')
    {
        %$item;
    }
    elsif ($ref eq 'ARRAY')
    {
        @$item;
    }
    else
    {
        error "hash or array received unknown element $ref";
    }
}

sub value
{
    while (@_)
    {
        my $name = shift;
        $packages{$current_package}->{$name} = shift;
    }
}

sub pr { if ($out) { print $out shift() } else { print shift } }

sub attach { $out = shift };

sub detach { $out = undef };

sub build_list
{
    [map
    {
        if (listp($_))
        {
            build_list(@$_);
        }
        elsif ($_ eq '#')
        {
            get;
        }
        elsif (/^\#.+/)
        {
            interpret_element substr($_,1);
            get;
        }
        else
        {
            $_;
        }
    } @_];
}



###



word { put $@ } '$@';

word
{
    my @word;
    my $name = read_word;
    return if read_error $name, ':', ';';
    my $word = read_word;
    return if read_error $word, ':', ';';
    while ($word ne ';')
    {
        if ($word eq ':' || $word eq '(')
        {
            execute_word $packages{main}->{$word};
        }
        elsif ($word eq '[' || $word eq '{' || $word eq '`')
        {
            execute_word $packages{main}->{$word};
            push @word, get;
        }
        elsif ($word eq '"')
        {
            execute_word $packages{main}->{$word};
            push @word, '"' . get() . '"';
        }
        else
        {
            push @word, $word;
        }
        $word = read_word;
        return if read_error $word, ':', ';';
    }

    colon $name, \@word;
} ':';

word
{
    my @list;
    my $word = read_word;
    return if read_error $word, '[', ']';

    while ($word ne ']')
    {
        if ($word eq ':' || $word eq '(')
        {
            execute_word $packages{main}->{$word};
        }
        elsif ($word eq '[' || $word eq '{' || $word eq '`')
        {
            execute_word $packages{main}->{$word};
            push @list, get;
        }
        elsif ($word eq '"')
        {
            execute_word $packages{main}->{$word};
            push @list, '"' . get() . '"';
        }
        else
        {
            if ($word eq '~reverse')
            {
                @list = reverse @list;
            }
            elsif ($word eq '~flatten')
            {
                push @list, @{pop @list};
            }
            elsif ($word =~ /^\,/)
            {
                interpret_element substr($word,1);
                push @list, get;
            }
            else
            {
                push @list, $word;
            }
        }
        $word = read_word;
        return if read_error $word, '[', ']';
    }
    
    put \@list;
} '[';

word
{
    my $char = getc $source;
    return if read_error $char, '"';
    my $text = '';
    while ($char ne '"')
    {
        $text .= $char;
        $char = getc $source;
        return if read_error $char, '"';
    }
    put $text;
} '"*';

word
{
    my $char = getc $source;
    return if read_error $char, '"';
    my $text = '';
    while ($char ne '"')
    {
        if ($char eq '\\')
        {
            $char = getc $source;
            return if read_error $char, '"';
        }
        $text .= $char;
        $char = getc $source;
        return if read_error $char, '"';
    }
    put $text;
} '"';

word
{
    my $char = getc $source;
    return if read_error $char, '`';
    my $text = '';
    while ($char ne '`')
    {
        $text .= $char;
        $char = getc $source;
        return if read_error $char, '`';
    }
    put $text;
} '`';

word { put $env } 'environment';
word { @stack = () } 'r';
word { put [@stack] } 'stack';
word
{
    my $element = get;
    my $ref = ref $element;
    my $type;
    if ($ref)
    {
        if ($ref eq 'ARRAY')
        {
            $ref = 'list';
        }
        elsif ($ref eq 'HASH')
        {
            $ref = 'hash';
        }
        elsif ($ref eq 'CODE')
        {
            $ref = 'sub';
        }
        elsif ($ref eq 'GLOB')
        {
            $ref = 'file';
        }
        my $found = find('represent.' . $ref) || find('represent.unknown');
        put $element;
        execute_word $found;
    }
    else
    {
        my $scalar = find('represent.scalar');
        if ($scalar)
        {
            put $element;
            execute_word $scalar
        }
        else
        {
            put $element;
        }
    }

} 'represent.element';

word
{
    my ($list, $code) = get2;
    my @list = hash_or_array($list);
    put [grep { defined } map { put $_; interpret_element $code; get } @list]; 
} 'map';

word
{
    my ($list, $separator) = get2;
    put(join($separator, @$list));
} 'join';

word
{
    my ($string, $separator) = get2;
    put([split($separator, $string)]);
} 'split';

word
{
    my $string = interpolate_string(get);
    put $string;
} 'format';

word
{
    my ($list, $code) = get2;
    my @pairs = hash_or_array($list);
    my @result;
    if (@pairs % 2)
    {
        error "map.pairs given a list not multiple of 2";
    }
    else
    {
        while (@pairs)
        {
            put(shift @pairs, shift @pairs);
            interpret_element $code;
            push @result, get;
        }
        put [grep { defined } @result];
    }
} 'map.pairs';

word
{
    my %hash;
    my $key = read_word;
    return if read_error $key, '{','}';
    while ($key ne '}')
    {
        my $value = read_word;
        return if read_error $value, '{','}';
        if ($value eq '}')
        {
            error "missing value for key:$key";
            return;
        }
        interpret_element $value;
        $hash{$key}=get;
        $key = read_word;
        return if read_error $key, '{','}';
    }
    put \%hash;
} '{';

word { if ($out) { print $out get() } else { print get } } '.';
word { if ($out) { print $out "\n" } else { print "\n" } } 'cr';
word { my ($one,$two) = get2; put($two,$one) } 'swap';
for my $color (qw/cyan red magenta blue black green white yellow/)
{
    word { pr color($color) } $color;
    word { pr color('on_' . $color) } 'on.' . $color;
    word { pr color('bright_' . $color) } 'bright.' . $color;
}
for my $color (qw/reset dark bold underline italic/)
{
    word { pr color($color) } $color;
}
word { pr color('reset') } 'reset';
word { pr color('dark') } 'dark';
word { my $fh = get; $fh->blocking(1) } 'blocking';
word { my $fh = get; $fh->blocking(0) } 'non.blocking';
word { put getc(get) } 'read.char';
word { my $fh = get; my $line = <$fh>; if (defined $line) { chomp $line } put $line } 'read.line';
word { my $fh = get; my $line = <$fh>; put $line } 'read.line*';
word { put defined(get) } 'defined';
word { put undef } 'nil';
word { my ($value, $ok) = get2; if ($value) { interpret_element $ok } } 'if';
word { my ($value, $ok, $else) = get3; if ($value) { interpret_element $ok } else { interpret_element $else } } 'if.else';
word { my $element = get; put($element,$element) } 'dup';
word { put get() eq get() } 'eq';
word { put get() ne get() } 'ne';
word { get } 'drop';
word { interpret_element get } 'eval';
word { eval { interpret_string get } } 'eval.string';
word { put(find(get)) } 'word';
word { put(ref(get)) } 'type';
word { my $element = get; if (listp $element) { put scalar @$element } else { put length $element } } 'len';
word { put $current_package } 'current.package';
word { pkg get } 'package';
word { my $name = get; my $str = ''; open my $buffer, '>', \$str or error "can't create text buffer" and return; put $buffer; &word(sub{ put $str },$name) } 'buffer';
word { my ($test, $code) = get2; interpret_element $test; while (get) { interpret_element $code; interpret_element $test } } 'while';
word { my ($test, $code) = get2; interpret_element $test; while (my $result = get()) { put $result; interpret_element $code; interpret_element $test } } 'while*';
word { put('"' . get . '"') } '$';

word
{
    my ($class, $name, $method) = get3;
    no strict 'refs';
    ${$class . '::'}{$name} = sub { put @_; interpret_element $method };
} 'method*';
word
{
    my ($class, $name, $method) = get3;
    no strict 'refs';
    ${$class . '::'}{$name} = sub { put @_; interpret_element $method; get };
} 'method';

word
{
    my ($class, $name, $value) = get3;
    no strict 'refs';
    ${$class . '::'}{$name} = $value;
} 'set.value';

word
{
    my ($class, $name, $value) = get3;
    no strict 'refs';
    my @list = hash_or_array($value);
    @{$class . '::' . $name} = @list;
} 'set.array';

word
{
    my ($class, $name, $value) = get3;
    no strict 'refs';
    my %hash = hash_or_array($value);
    %{$class . '::' . $name} = %hash;
} 'set.hash';

word
{
    my ($obj, $class) = get2;
    if (not ref $obj)
    {
        put(bless \$obj, $class);
    }
    else
    {
        put(bless $obj, $class);
    }
} 'bless';

word
{
    my ($class, $name) = get2;
    no strict 'refs';
    put ${$class . '::'}{$name};
} 'get.value';
# ty mauke <3
word { my ($class,$name) = get2; no strict 'refs'; put(*{\${$class . '::'}{$name}}{CODE}) } 'get.sub';
    
word
{
    my ($class, $name) = get2;
    no strict 'refs';
    #my @list = @{*{\${$class . '::'}{$name}}{ARRAY}};
    my @list = @{$class . '::' . $name};
    put \@list
} 'get.array';

word
{
    my ($class, $name) = get2;
    no strict 'refs';
    my %hash = %{$class . '::' . $name};
    put \%hash;
} 'get.hash';


word { eval get } 'perl';
word { put(scalar eval get) } 'perl*';
word { my ($name,$code) = get2; my $sub = eval "sub { $code }"; if ($@) { error "perl.word $@"; return } value($name,$sub) } 'perl.word';
word { put(@{get()}) } 'flatten';
word { execute_word get } 'execute.word';
word { my ($obj, $method) = get2; $obj->$method() } 'method.call~';
word { my ($obj, $method) = get2; put($obj->$method()) } 'method.call';
word { my ($args,$obj, $method) = get3; $obj->$method(hash_or_array($args)) } 'method.call*~';
word { my ($args, $obj, $method) = get3; put($obj->$method(hash_or_array($args))) } 'method.call*';
word { put(get()->()) } 'sub.call';
word { get()->() } 'sub.call~';
word { my @args = hash_or_array(get()); put(get()->(@args)) } 'sub.call*';
word { my @args = hash_or_array(get()); get()->(@args) } 'sub.call*~';
word { my ($code,$number) = get2; put($code->(getn($number))) } 'sub.n';
word { my ($code,$number) = get2; $code->(getn($number)) } 'sub.n~';
word { my $code = get; put sub { interpret_element $code } } 'sub';
word { my $word = find(get()); if ($word) { put($word->{env}) } else { put undef } } 'word.environment';
word { put(get()->{env}) } 'word.environment*';
word { my ($hash, $key, $value) = get3; $hash->{$key}=$value } 'set.key';
word { my $key = get; put get()->{$key} } 'get.key';
word { my ($hash, $key, $value) = get3; $hash->[$key]=$value } 'set.element';
word { my $key = get; put get()->[$key] } 'get.element';
word { my ($value, $hash, $key) = get3; $hash->{$key}=$value } 'set.key*';
word { my ($value, $hash, $key) = get3; $hash->[$key]=$value } 'set.element*';
word { my $key = get; delete get()->{$key} } 'remove.key';
word { my $key = get; splice @{get()},$key,1 } 'remove.element';
word { my $word = get; delete $packages{$current_package}->{$word}  } 'remove.word';
word { my ($package,$word) = get2; delete $packages{$package}->{$word}  } 'remove.word*';
word { my $hash = get; %$hash = () } 'reset.hash';
word { my $hash = get; @$hash = () } 'reset.list';
word { my $element = get; put(++$element) } '++';
word { my $element = get; put(--$element) } '--';
word { my $element = get; exec $element } 'exec';
word { my $element = get; system $element } 'system';
word { put(read_word) } 'source.word';
word { my $line = <$source>; if (defined $line) { chomp $line; put($line) } else { put '' } } 'source.line';
word { my $line = <$source>; if (defined $line) { put($line) } else { put '' } } 'source.line*';
word { put [] } '[]';
word { put {} } '{}';
word { my $obj = get; put $$obj } 'deref';
word { put(build_list(@{get()})) } '#';
word { my ($name, $value) = get2; value $name, $value } 'set';
word { my ($value, $name) = get2; value $name, $value } 'def';
word { my ($name, $value) = get2; colon $name, $value } 'colon';
word { put('"' . get() . '"') } '$';
word { put \%packages } 'words';
word { my ($code,$times) = get2; for(1..$times) { interpret_element $code } } 'do.times';
word { my ($element, $times) = get2; put(($element) x $times) } 'duplicate';
word { my ($code,$times) = get2; for my $time (1..$times) { put $time; interpret_element $code } } 'do.times*';
word { my ($list, $code) = get2; for my $element (@$list) { put $element; interpret_element $code } } 'do.list';
word
{
    my ($list, $code) = get2;
    my @list = hash_or_array($list);
    if (@list % 2)
    {
        error 'do.pairs given a list with number of elements not multiple of two';
        return;
    }
    while (@list)
    {
        put(shift @list, shift @list);
        interpret_element $code;
    }
} 'do.pairs';

word
{
    my ($list, $code) = get2;
    my @list = hash_or_array($list);
    if (@list % 2)
    {
        error 'do.pairs given a list with number of elements not multiple of two';
        return;
    }
    while (@list)
    {
        my $element = shift @list;
        put(shift @list, $element);
        interpret_element $code;
    }
} 'do.pairs*';

word
{
    my ($list, $code, $number) = get3;
    my @list = hash_or_array($list);
    if (@list % $number)
    {
        error "do.n given a list with number of elements not multiple of $number";
        return;
    }
    while (@list)
    {
        put(splice @list, 0, $number);
        interpret_element $code;
    }
} 'do.n';

word
{
    my ($list, $code, $number) = get3;
    my @list = hash_or_array($list);
    if (@list % $number)
    {
        error "do.n given a list with number of elements not multiple of $number";
        return;
    }
    while (@list)
    {
        put(reverse(splice @list, 0, $number));
        interpret_element $code;
    }
} 'do.n*';
word { my ($from, $to) = get2; put [$from..$to] } 'range';

word { my ($one,$two) = get2; put($one+$two) } '+';
word { my ($one,$two) = get2; put($one-$two) } '-';
word { my ($one,$two) = get2; put($one*$two) } '*';
word { my ($one,$two) = get2; put($one/$two) } '/';

word { put(sum0(@{get()})) } 'sum';

word { my ($string, $start) = get2; my $str = substr($string, $start); put $str } 'substr';
word { my ($string, $start, $end) = get3; my $str = substr($string, $start, $end); put $str } 'substring';

word { @stack = @{get()} } 'set.stack';

word { my $module = get; eval "use $module" } 'use';
word { my ($module, $parent) = get2; no strict 'refs'; push @{$module . '::ISA'}, $parent; } 'inherit';
word { my $module = get; no strict 'refs'; my @parents = @{$module . '::ISA'}; put \@parents } 'inheritance';

word { my ($one,$two,$three) = get3; put($two,$three,$one) } 'rot';
word { my ($one,$two,$three) = get3; put($three,$one,$two) } 'rot*';   
word { my ($one,$two) = get2; put($two) } 'nip';

word { my $name = get; open my $file, '<', $name or error "file.string can't open file $name" and return; read $file, my $string, -s $file; put $string } 'file.string';
word { my ($url, $port) = get2; put(IO::Socket::INET->new("$url:$port")) } 'client.socket';
word { put(IO::Socket::INET->new(Listen => 3, LocalPort => get())) } 'server.socket';
word { error get } 'error';
word { attach get } 'attach';
word { detach } 'detach';
word { pr ":D\n" } ':D';
word { my ($string, $fh) = get2; $fh->print($string) } 'write';
word { my ($fh, $bytes) = get2; $fh->read(my $read, $bytes); put $read } 'read';
word { my ($fh, $bytes, $offset) = get3; $fh->read(my $read, $bytes, $offset); put $read } 'read*';
word { my $file = get; tie my %hash, 'DB_File', $file or error "can't create magic hash with $file as database" and return; put \%hash } 'magic.hash';
word { tied(%{get()})->sync } 'magic.flush';
word { my $line = get; chomp $line; put $line } 'chomp';
word { my $filename = get; put(-s $filename) }  'file.size';
word { my ($list1, $list2) = get2; my @list; for (my $i = 0; $i < @$list1; $i++) { push @list, $list1->[$i], $list2->[$i] } put \@list } 'zip';
word { my ($list2, $list1) = get2; my @list; for (my $i = 0; $i < @$list1; $i++) { push @list, $list1->[$i], $list2->[$i] } put \@list } 'zip*';
word { my %hash = hash_or_array get; put \%hash } 'hash';
word { put $queue } 'queue';
word { $queue->enqueue(get) } 'enqueue';
word { put($queue->dequeue_nb(1)) } 'dequeue';
word { my $code = get; async { interpret_element $code }->detach } 'async';
word { my $hash = get; put [keys %$hash] } 'keys';
word { my $hash = get; put [values %$hash] } 'values';
word { put [keys %packages] } 'packages';
word { put [keys %{$packages{get()}}] } 'package.words';
word { put(get->[0]) } 'car';
word { my @copy = @{get()}; shift @copy; put \@copy } 'cdr';
word { put shift @{get()} } 'car!';
word { my $list = get; shift @$list; put $list } 'cdr!';
word { put scalar @stack } 'stack.len';
word { put [getn(get)] }  'list';
word { put [sort @{get()}] } 'sort';
word { interpret_file(get) } 'load.file';
word { my $code = get; my @list; interpret_element $code; my $result = get; while ($result) { push @list, $result; interpret_element $code; $result = get } put \@list } 'collect';
word { my $code = get; my @list; interpret_element $code; my $result = get; while (defined $result) { push @list, $result; interpret_element $code ; $result = get } put \@list } 'collect*';
word { interpret_atom get } 'eval.atom';
word { put listp(get) } 'listp';
word { put not get()->[0] } 'empty';
word
{
    my $file = get;
    my $pkg = $current_package;
    $file =~ /^(.*)\..+$/;
    if ($1)
    {
        pkg $1;
        interpret_file($file);
        pkg $pkg;
    }
    else
    {
        error "can't get module name";
    }
} 'load.package';

word
{
    my $file = get;
    my $pkg = $current_package;
    $file =~ /^(.*)\..+$/;
    if ($1)
    {
        pkg $1;
        interpret_file($file);
        pkg $pkg;
        parent $1;
    }
    else
    {
        error "can't get module name";
    }
} 'load.module';

word { parent get } 'parent';
word { put(-e get()) } 'file.exists';
word { my ($string,$match) = get2; my $result = () = ($string =~ /$match/g); put $result } 'matches'; 

word
{
    my ($value, $cases) = get2;
    my $done;
    my $t;
    my @cases = @$cases;
    while (@cases)
    {
        my ($regex, @code) = @{shift(@cases)};
        if ($regex eq 't')
        {
            $t = \@code;
            next;
        }
        my $match = eval "\$value =~ $regex";
        if ($match)
        {
            $done = 1;
            interpret_element \@code;
            last;
        }
    }
    if ($t and not $done)
    {
        interpret_element $t;
    }
} 'regex.case';

word
{
    my ($value, $cases) = get2;
    my $done;
    my $t;
    my @cases = @$cases;
    while (@cases)
    {
        my ($regex, @code) = @{shift(@cases)};
        if ($regex eq 't')
        {
            $t = \@code;
            next;
        }
        my $match = eval "\$value =~ $regex";
        if ($match)
        {
            $done = 1;
            interpret_element \@code;
        }
    }
    if ($t and not $done)
    {
        interpret_element $t;
    }
} 'regex.case*';

word
{
    my ($value, $cases) = get2;
    my $done;
    my $t;
    my @cases = @$cases;
    while (@cases)
    {
        my ($test, @code) = @{shift(@cases)};
        if ($test eq 't')
        {
            $t = \@code;
            next;
        }
        if ($test =~ /^\,/)
        {
            interpret_element substr($test,1);
            $test = get;
        }
        if ($test eq $value)
        {
            $done = 1;
            interpret_element \@code;
            last;
        }
    }
    if ($t and not $done)
    {
        interpret_element $t;
    }
} 'case';

word
{
    my ($value, $cases) = get2;
    my @cases = @$cases;
    my $t;
    my $done;
    while (@cases)
    {
        my $cond = shift @cases;
        my $code = shift @cases;
        if (not ref $cond and $cond eq 't')
        {
            $t = $code;
            next;
        }

        put $value;
        interpret_element $cond;
        if (get())
        {
            interpret_element $code;
            $done = 1;
            last;
        }
    }
    if ($t and not $done)
    {
        interpret_element $t;
    }
} 'cond';

word
{
    my ($value, $cases) = get2;
    my @cases = @$cases;
    my $t;
    my $done;
    while (@cases)
    {
        my $cond = shift @cases;
        my $code = shift @cases;
        if (not ref $cond and $cond eq 't')
        {
            $t = $code;
            next;
        }

        put $value;
        interpret_element $cond;
        if (get())
        {
            interpret_element $code;
            $done = 1;
        }
    }
    if ($t and not $done)
    {
        interpret_element $t;
    }
} 'cond*';

word { put(get()<get()) } 'greater';
word { put(get()>get()) } 'less';
word { put(get()<=get()) } 'greater.or.equal';
word { put(get()>=get()) } 'less.or.equal';

word
{
    my $names = get;
    for my $name (reverse @$names)
    {
        $env->{$name} = get;
    }
} 'bind';

word
{
    my $names = get;
    for my $name (@$names)
    {
        $env->{$name} = get;
    }
} 'bind*';

word
{
    my ($list, $names) = get2;
    my @list = @$list;
    if (@list != @$names)
    {
        error 'bind.list given a list of name with different number of values of the list to bind';
        return;
    }
    for my $name (@$names)
    {
        $env->{$name} = shift @list;
    }
} 'bind.list';


word
{
    my ($list, $names) = get2;
    my @list = @$list;
    if (@list != @$names)
    {
        error 'bind.list given a list of name with different number of values of the list to bind';
        return;
    }
    for my $name (@$names)
    {
        $env->{$name} = pop @list;
    }
} 'bind.list*';

word
{
    my ($hash, $names) = get2;
    for my $name (@$names)
    {
        if (exists $hash->{$name})
        {
            $env->{$name} = $hash->{$name};
        }
        else
        {
            $env->{$name} = sub { put undef };
        }
    }
} 'bind.hash';

word
{
    my ($hash, $names) = get2;
    for my $name (@$names)
    {
        if (exists $hash->{$name})
        {
            $env->{$name} = $hash->{$name};
        }
        else
        {
            $env->{$name} = sub { put '' };
        }
    }
} 'bind.hash*';

word
{
    my ($value, $code) = get2;
    for my $code (@$code)
    {
        put $value;
        interpret_element $code;
    }
} 'with.value';

word
{
    my ($code, $values) = get2;
    for my $value (@$values)
    {
        put $value;
        interpret_element $code;
    }
} 'with.code';

word
{
    my ($code, $values) = get2;
    for my $value (@$values)
    {
        interpret_element $value;
        interpret_element $code;
    }
} 'with.code*';

word
{
    my $list = get;
    my $result;
    my @list = @$list;
    my $last = pop @list;
    for my $code (@list)
    {
        interpret_element $code;
        $result = get;
        last unless $result;
    }
    if ($result)
    {
        interpret_element $last;
    }
} 'and*';

word
{
    my $list = get;
    my $result;
    my @list = @$list;
    my $last = pop @list;
    for my $code (@list)
    {
        interpret_element $code;
        $result = get;
        last if $result;
    }
    unless ($result)
    {
        interpret_element $last;
    }
} 'or*';


word
{
    my $list = get;
    my $result;
    for my $code (@$list)
    {
        interpret_element $code;
        $result = get;
        last unless $result;
    }
    put $result;
} 'and';

word
{
    my $list = get;
    my $result;
    for my $code (@$list)
    {
        interpret_element $code;
        $result = get;
        last if $result;
    }
    put $result;
} 'or';

word { my $num = get;put($num =~ /^\-?\d+\.?\d*$/ || $num =~ /^0x[a-f0-9]/i || undef) } 'number';

word { put(not(get)) } 'not';

word { my ($one,$two) = get2; put($one.$two) } '..';
word { my ($one,$two) = get2; put [hash_or_array($one),hash_or_array($two)] } '...';
word { my ($one,$two) = get2; put {hash_or_array($one),hash_or_array($two)} } '....';

word { put(abs get) } 'abs';
word { put chr get } 'chr';
word { put ord get } 'ord';
word { pr(chr(get)) } 'emit';

word { my $list = get; put($list->[rand(@$list)]) } 'pick.random';

word
{
    my $name = get;
    if (exists $packages{$current_package}->{$name} and not ref $packages{$current_package}->{$name})
    {
        $packages{$current_package}->{$name}++
    }
    else
    {
        error "$name can't be incremented " . ref $packages{$current_package}->{$name}
    }
} 'increment'; 

word
{
    my $name = get;
    if (exists $env->{$name} and not ref $env->{$name})
    {
        $env->{$name}++;
    }
    else
    {
        error "$name can't be incremented " . ref $env->{$name};
    }
} 'inc';

word
{
    my $name = get;
    if (exists $env->{$name} and not ref $env->{$name})
    {
        put(++$env->{$name});
    }
    else
    {
        error "$name can't be incremented " . ref $env->{$name};
    }
} 'inc*';

word
{
    my $name = get;
    if (exists $env->{$name} and not ref $env->{$name})
    {
        $env->{$name}--;
    }
    else
    {
        error "$name can't be decremented " . ref $env->{$name};
    }
} 'dec';

word
{
    my $name = get;
    if (exists $env->{$name} and not ref $env->{$name})
    {
        put(--$env->{$name});
    }
    else
    {
        error "$name can't be decremented " . ref $env->{$name};
    }
} 'dec*';

word { my $num = get; put rand($num) } 'rand';
word { put int get } 'int';
word { put max(@{get()}) } 'max';
word { put min(@{get()}) } 'min';
word { my ($list, $element) = get2; push @$list, $element } 'push';
word { my ($list, $element) = get2; push @$list, $element } 'unshift';
word { put (pop @{get()}) } 'pop';
word { put (shift @{get()}) } 'shift';
word { my ($list,$code) = get2; put [grep { put($_); interpret_element $code; get() } @$list] } 'grep';

value
    stdin => $stdin,
    stdout => $stdout,
    stderr => $stderr,
    '@INC' => \@INC,
    '%INC' => \%INC,
    env => \%ENV,
    space => ' ',
    newline => "\n",
    tab => "\t";


interpret_string <<'OH';
: represent.scalar 
    dup defined [ dup ' eq [ drop '' ] [ dup number not [ ''~a' format s/\n/~%/gr ] if ] if.else ] [ drop 'nil ] if.else ;
: represent.unknown type ;
: represent.list
    [ represent.element ] map space join '[~a] format ;
: represent.hash
    [ represent.element swap '~a:~a format ] map.pairs space join '{~a} format ;
: represent.sub drop 'code ;
: .s stack represent.element . cr ;
: .cr . cr ;
: sp space . ;
: prompt cr " > " . ;
: file.lines file.string newline matches ;
: lines newline split ;
: colors [ red yellow blue magenta green white ] ;
: collect.times :times :code [ ] [ code eval 1 list ... ] times do.times ;
: binding :args :name name get.sub [ # #args sub.n ] # name swap colon ;
: binding~ :args :name name get.sub [ # #args sub.n~ ] # name swap colon ;
: binding* :args :ourname :name name get.sub [ # #args sub.n ] # ourname swap colon ;
: binding*~ :args :ourname :name name get.sub [ # #args sub.n~ ] # ourname swap colon ;
: bindings :args :module [ module swap args binding ] do.list ;
: bindings~ :args :module [ module swap args binding~ ] do.list ;
: bindings* :args :module [ :fun :ourname module ourname fun args binding* ] 2 do.n ;
: bindings*~ :args :module [ :fun :ourname module ourname fun args binding*~ ] 2 do.n ;
OH

1;


;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
(package-initialize)

(setq backup-inhibited t)
(setq auto-save-default nil)
(global-set-key (kbd "C-x C-c") (lambda () (interactive) (kill-emacs)))
(setq c-default-style "ellemtel")
(setq-default indent-tabs-mode nil)
(setq-default tab-width 3)
(setq indent-line-function 'insert-tab)
(defun indent-file () (interactive) (indent-region 0 (point-max)))
(add-to-list 'auto-mode-alist
             '("\\.oh\\'" . (lambda ()
                              (electric-indent-mode 0))))

